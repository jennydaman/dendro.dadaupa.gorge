{ config, lib, pkgs, ... }:
let
  unstable = import
    (builtins.fetchTarball https://github.com/nixos/nixpkgs/tarball/master)
    # reuse the current configuration
    { config = config.nixpkgs.config; };
in
{
  services.kubo = {
    # Giving up on using IPFS. It uses too much CPU.
    enable = false;
    localDiscovery = true;
    settings = {
      Addresses = {
        Gateway = "/ip4/0.0.0.0/tcp/1475";
        API = [ "/ip4/0.0.0.0/tcp/1476" ];
      };
      API = {
        HTTPHeaders = {
          "Access-Control-Allow-Origin" = [ "http://dendro:1476" "https://ipfs-private.jennin.xyz" ];
          "Access-Control-Allow-Methods" = [ "PUT" "POST" ];
        };
      };
      # improves the speed of DHT (letting peers know what files I have), increases CPU and RAM usage
      # https://github.com/ipfs/kubo/blob/master/docs/config.md#routingaccelerateddhtclient
      Routing.AcceleratedDHTClient = true;
    };
  };

  services.invidious = {
    enable = true;
    port = 1444;
    package = unstable.invidious;
    settings.db.user = "invidious";  # needed since upgrade to NixOS 24.05 from 23.11
    extraSettingsFile = "/etc/nixos/secrets/invidious.json";
    sig-helper = {
      enable = true;
      package = unstable.inv-sig-helper;
    };
  };

  services.scrutiny = {
    enable = true;
    collector = {
      enable = true;
      settings = {
        host.id = "${config.networking.hostName}";
        devices = [ { device = "/dev/sda"; } ];
      };
    };
    settings.web.listen.port = 15347;
  };

  services.polaris = {
    enable = true;
    settings = {
      reindex_every_n_seconds = 60 * 60 * 24 * 7;
      mount_dirs = [
        {
          name = "Music";
          source = "/var/media/Music";
        }
      ];
    };
  };

  # Resource configuration
  # ----------------------------------------
  
  systemd.services = {
    # ipfs = {
    #   requires = [ "var-lib-ipfs.mount" ];
    #   serviceConfig = {
    #     CPUQuota = "100%";
    #     MemoryMax = "3G";
    #     MemorySwapMax = "3G";
    #     IOAccounting = true;
    #     IOReadBandwidthMax = "/dev/sda 10M";
    #   };
    # };

    invidious = {
      serviceConfig = {
        CPUQuota = "100%";
        MemoryMax = "3G";
        MemorySwapMax = "3G";
      };
    };

    postgresql = {
      requires = [ "var-lib-postgresql.mount" ];
    };

    polaris = {
      requires = [ "var-media.mount" ];
    };
  };
}
