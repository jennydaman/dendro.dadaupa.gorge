# based on https://discourse.nixos.org/t/sssd-with-sudo-support/30748

{ config, lib, pkgs, ... }:
{
  services = {
    sssd = {
      sshAuthorizedKeysIntegration = true;
      enable = true;
      config = ''
# https://goauthentik.io/integrations/services/sssd/

[nss]
filter_groups = root
filter_users = root
reconnection_retries = 3

[sssd]
config_file_version = 2
reconnection_retries = 3
domains = auth.jennin.xyz

services = nss, pam, ssh, sudo

[pam]
reconnection_retries = 3

[domain/auth.jennin.xyz]
cache_credentials = True
id_provider = ldap
chpass_provider = ldap
auth_provider = ldap
access_provider = ldap
ldap_uri = ldaps://pyro.dadaupa.gorge:6636

# allow self-signed certificate
ldap_tls_reqcert = allow

ldap_schema = rfc2307bis
ldap_search_base = dc=jennin,dc=xyz
ldap_user_search_base = ou=users,dc=jennin,dc=xyz
ldap_group_search_base = dc=jennin,dc=xyz

ldap_user_object_class = user
ldap_user_name = cn
ldap_group_object_class = group
ldap_group_name = cn

# Optionally, filter logins to only a specific group
ldap_access_order = filter
ldap_access_filter = memberOf=cn=ldap_users,ou=groups,dc=jennin,dc=xyz

ldap_default_bind_dn = cn=ldap_admin,ou=users,dc=jennin,dc=xyz
ldap_default_authtok = '' + (builtins.readFile ./secrets/authentik_ldap_token) + ''

enumerate = true
override_shell = /run/current-system/sw/bin/zsh
      '';
    };
      nscd.config = ''
        enable-cache hosts no
        enable-cache passwd no
        enable-cache group no
        enable-cache netgroup no
        enable-cache services no
      '';
  };

  # environment.persistence."/persist" = {
  #   hideMounts = true ;
  #   directories = [
  #     "/var/lib/sss"                     # sssd
  #   ];
  # };

  ## TODO - Find a way to insert something into nsswitch rather than overwriting
  environment.etc."nsswitch.conf".text = lib.mkForce ''
passwd:    files sss systemd
group:     files sss [success=merge] systemd
shadow:    files sss
sudoers:   files sss

hosts:     mymachines resolve [!UNAVAIL=return] files myhostname dns
networks:  files

ethers:    files
services:  files sss
protocols: files
rpc:       files
  '';

  security.pam.services.systemd-user.makeHomeDir = true;

  systemd.services.sssd.serviceConfig = {
    Restart = "on-failure";
    RestartSec = 10;
  };
}
