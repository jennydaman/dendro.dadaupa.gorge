{ config, lib, pkgs, modulesPath, ... }:

{
  security.sudo.enable = false;
  security.sudo-rs.enable = true;
  security.sudo-rs.extraRules = [
    {
      users = [ "btrbk-pyro" ];
      commands = [
        { command = "${pkgs.btrfs-progs}/bin/btrfs";       options = [ "NOPASSWD" ]; }
        { command = "/run/current-system/sw/bin/btrfs";    options = [ "NOPASSWD" ]; }
        { command = "/run/current-system/sw/bin/readlink"; options = [ "NOPASSWD" ]; }
      ];
    }
  ];
}
