{
  disko.devices = {
    disk = {
      sda = {
        type = "disk";
        device = "/dev/sda";
        content = {
          type = "gpt";
          partitions = {
            mbr = {
              size = "1M";
              type = "EF02"; # for grub MBR
            };
            boot = {
              size = "1G";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [ "defaults" ];
              };
            };
            luks = {
              size = "100%";
              content = {
                type = "luks";
                name = "crypted";
                settings = {
                  allowDiscards = true;
                  fallbackToPassword = true;
                };
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ];
                  subvolumes = {
                    "/root" = {
                      mountpoint = "/";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/home" = {
                      mountpoint = "/home";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/nix" = {
                      mountpoint = "/nix";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/var-cache" = {
                      mountpoint = "/var/cache";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/var-tmp" = {
                      mountpoint = "/var/tmp";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/var-log" = {
                      mountpoint = "/var/log";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/backups" = {
                      mountpoint = "/var/backups";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/postgresql" = {
                      mountpoint = "/var/lib/postgresql";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/ipfs" = {
                      mountpoint = "/var/lib/ipfs";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/openobserve" = {
                      mountpoint = "/var/lib/openobserve";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/media" = {
                      mountpoint = "/var/media";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/swap" = {
                      mountpoint = "/.swapvol";
                      swap.swapfile.size = "8G";
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };

  # to have a correct log order
  fileSystems."/var/log".neededForBoot = true;
}

