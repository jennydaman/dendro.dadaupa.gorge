{ config, lib, pkgs, ... }:
{
  virtualisation.oci-containers.backend = "podman";
  virtualisation.oci-containers.containers = {
    openobserve = {
      image = "public.ecr.aws/zinclabs/openobserve:v0.12.1";
      autoStart = false;  # don't really use OpenObserve, it just eats disk space
      ports = [ "5080:5080" ];
      volumes = [
        "/var/lib/openobserve:/data"
      ];
      environment = { ZO_DATA_DIR = "/data"; };
      environmentFiles = [ /etc/nixos/secrets/openobserve.env ];
      extraOptions = [ "--cpus=0.25" "--memory=1g" ];
    };

    yt-dlp-webui = let
      configFile = (pkgs.formats.yaml { }).generate "yt-dlp-webui-config-yml" {
        port = 3033;
      }; in
    {
      image = "ghcr.io/marcopeocchi/yt-dlp-web-ui:latest";
      autoStart = true;
      cmd = [
        "--session" "/session"
      ];
      # extraOptions = [ "--pull=always" ];
      user = "${toString config.users.users.yt-dlp-webui.uid}:${toString config.users.groups.media.gid}";
      ports = [ "3033:3033" ];
      volumes = [
        "/var/media/YouTube:/downloads"
        "/var/lib/yt-dlp-webui/session:/session"
        "/var/lib/yt-dlp-webui/config:/config"
        "${configFile}:/config/config.yml:ro"
      ];
    };
  };
  systemd.services.podman-openobserve = {
    requires = [ "var-lib-openobserve.mount" ];
  };
  systemd.services.podman-yt-dlp-webui = {
    requires = [ "var-media.mount" ];
  };
  systemd.tmpfiles.rules = [
    "v /var/lib/yt-dlp-webui/config  770 yt-dlp-webui media"
    "v /var/lib/yt-dlp-webui/session 770 yt-dlp-webui media"
  ];
  users.users.yt-dlp-webui = {
    isSystemUser = true;
    uid = 992;
    group = "media";
    home = "/var/lib/yt-dlp-webui";
  };
  users.groups.media = {
    gid = 988;
  };
}
