{ config, lib, pkgs, ... }:
{
  systemd.services.miniserve-media = {
    enable = true;
    requires = [ "var-media.mount" ];
    serviceConfig = {
      ExecStart = "${pkgs.miniserve}/bin/miniserve -p 12363 -W /var/media";
      DynamicUser = "yes";
      WorkingDirectory = "/var/media";
      ReadOnlyPaths = "/var/media";

      CPUQuota = "50%";
      MemoryMax = "1G";

      NoNewPrivileges = "true";
      PrivateDevices = "true";
      PrivateTmp = "true";
      ProtectHome = "true";
      ProtectProc = "invisible";
      ProtectSystem = "strict";
      ProtectKernelTunables = "true";
      ProtectKernelLogs = "true";
      ProtectControlGroups = "true";
      ProtectClock = "true";
      LockPersonality = "true";
      RestrictNamespaces = "true";
      RestrictRealtime = "true";
      MemoryDenyWriteExecute = "true";
    };
    wantedBy = [ "multi-user.target" ];
  };
}
