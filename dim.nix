{ config, lib, pkgs, ... }:
{
  # NOTE: not using dim.
  # - As of 2024-10-06, dim project development has slowed down. The https://dusklabs.io/ website is gone,
  #   and documentation is scarce.
  # - Not as convenient to use as a plain HTTP file server or SCP. Using dim, it is necessary to first
  #   register a folder as a library, then match each file to a known series/movie from themoviedb.org
  systemd.services.dim = {
    enable = false;
    requires = [ "var-media.mount" ];
    serviceConfig = {
      ExecStart = "${pkgs.dim}/bin/dim";
      User = "dim";
      Group = "dim";
      StateDirectory = "dim";
      WorkingDirectory = "/var/lib/dim";
      ReadOnlyPaths = "/var/media";

      CPUQuota = "100%";
      MemoryMax = "2G";

      DeviceAllow = "/dev/dri/renderD128";
      DevicePolicy = "closed";

      PrivateTmp = "true";
      NoNewPrivileges = "true";
      ProtectHome = "true";
      ProtectSystem = "strict";
      ProtectKernelTunables = "true";
      ProtectKernelLogs = "true";
      ProtectControlGroups = "true";
      ProtectClock = "true";
      LockPersonality = "true";
      RestrictNamespaces = "true";
      RestrictRealtime = "true";
      MemoryDenyWriteExecute = "true";
    };
    wantedBy = [ "multi-user.target" ];
  };

  users.users.dim = {
    isSystemUser = true;
    group = "dim";
    home = "/var/lib/dim";
    description = "Dim Media Server";
  };
  users.groups.dim = {};
}
